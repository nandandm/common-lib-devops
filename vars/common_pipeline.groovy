import org.jenkinsci.plugins.pipeline.modeldefinition.Utils
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
def call(body) {
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    println("common_pipeline [start]")
}
